# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022
NORMAL="\e[0m"
RED="\e[1;31m"
GREEN="\e[1;32m"

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi
trap "" 2
while [ 1 ]
do
   tput clear
   nc -zvw 1 ajaxfanzone.nl 80 >/dev/null 2>&1
   if [ $? -eq 0 ]
   then
     WEB="$GREEN Webpage ok $NORMAL"
   else
     WEB="$RED Webpage nok $NORMAL"
   fi
   echo ""
   echo "            ********************************************"
   echo "             Ajaxfanzone.nl: Opstelling genereren"
   echo -e "            $WEB"
   echo "            ********************************************"
   echo ""
   echo ""
   echo "            1.  Genereer een opstelling"
   echo ""
   echo ""
   echo "            2.  Exit"
   echo ""
   echo -n "                Enter Choice : "
   read choice
   case $choice in
        1) ./Maakplaatje.sh;echo "Plaatje gemaakt. Afsluiten verbinding.";sleep 3;exit;;
        2)

      exit ;;
        7) tput clear;echo -e "De header laat zien of de servers (webpage en ftp) te benaderen zijn.\n$GREEN Groen $NORMAL is goed en $RED rood $NORMAL niet zo.";echo -e "Optie 1 haalt een lijst van bestanden op van beide ftp-server en vergelijkt die.\nGroen is geen verschil.";echo "Dit wordt ook automatisch gedaan en daardoor wordt een mail gestuurd.";echo -e "Optie2 haalt de bestanden uit de verschillenlijst op en \nprobeert dit van beide servers.";echo "Optie 3 brengt de verschillen naar de andere ftpserver."; echo -e "Optie 5 sluit dit af en zorgt dat er weer automatisch gemaild wordt als er \neen verschil is.";echo -e "Geef enter:\c";read;;
        *) echo -e "  Ben je scheel? Dit staat niet in het menu." ;sleep 3;;
   esac
done
