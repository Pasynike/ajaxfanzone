#!/bin/bash
#V1.0
# Wat standaard variabelen zodat bij elk systeem de verhouding klopt
T="-fuzz 1% -transparent white"
S=90
LIJST=$(cd AjaxSpeler;ls *.jpg|sed s/'.jpg'/''/g)
# Einde variabelen
echo -e "Welk systeem (4-3-3) (5-3-2) (4-4-2) (3-4-3):4-3-3\b\b\b\b\b\c"
read SYS
if [ "$SYS" = "" ]
then
  SYS=4-3-3
fi
case $SYS in
4-3-3)
#------begin Systeem 4-3-3-
printf "%s\n" $LIJST|column
 while [ ! -e "AjaxSpeler/$KEEPER.jpg" ]; do echo -e "Wie is keeper($(cat KEEPER)):\c";read KEEPER;if [ "$KEEPER" = "" ]; then KEEPER=$(cat KEEPER);fi;done
 while [ ! -e "AjaxSpeler/$LB.jpg" ]; do echo -e "Wie is LB($(cat LB)):\c";read LB;if [ "$LB" = "" ]; then LB=$(cat LB);fi;done
 while [ ! -e "AjaxSpeler/$B1.jpg" ]; do echo -e "Wie is B1($(cat B1)):\c";read B1;if [ "$B1" = "" ]; then B1=$(cat B1);fi;done
 while [ ! -e "AjaxSpeler/$B2.jpg" ]; do echo -e "Wie is B2($(cat B2)):\c";read B2;if [ "$B2" = "" ]; then B2=$(cat B2);fi;done
 while [ ! -e "AjaxSpeler/$RB.jpg" ]; do echo -e "Wie is RB($(cat RB)):\c";read RB;if [ "$RB" = "" ]; then RB=$(cat RB);fi;done
 while [ ! -e "AjaxSpeler/$LH.jpg" ]; do echo -e "Wie is LH($(cat LH)):\c";read LH;if [ "$LH" = "" ]; then LH=$(cat LH);fi;done
 while [ ! -e "AjaxSpeler/$CV.jpg" ]; do echo -e "Wie is CV($(cat CV)):\c";read CV;if [ "$CV" = "" ]; then CV=$(cat CV);fi;done
 while [ ! -e "AjaxSpeler/$RH.jpg" ]; do echo -e "Wie is RH($(cat RH)):\c";read RH;if [ "$RH" = "" ]; then RH=$(cat RH);fi;done
 while [ ! -e "AjaxSpeler/$LV.jpg" ]; do echo -e "Wie is LV($(cat LV)):\c";read LV;if [ "$LV" = "" ]; then LV=$(cat LV);fi;done
 while [ ! -e "AjaxSpeler/$SP.jpg" ]; do echo -e "Wie is SP($(cat SP)):\c";read SP;if [ "$SP" = "" ]; then SP=$(cat SP);fi;done
 while [ ! -e "AjaxSpeler/$RV.jpg" ]; do echo -e "Wie is RV($(cat RV)):\c";read RV;if [ "$RV" = "" ]; then RV=$(cat RV);fi;done
NAAM="$(date +"%Y%m%d")opstelling"
convert veldwit.png        \( AjaxSpeler/$KEEPER.jpg -resize $S -repage +250+280 $T \) \( AjaxSpeler/"$LB.jpg" -resize $S -repage +40+240 $T \) \( AjaxSpeler/"$B1.jpg" -resize $S -repage +150+220 $T \)  \( AjaxSpeler/"$B2.jpg" -resize $S -repage +360+220 $T \) \( AjaxSpeler/"$RB.jpg" -resize $S -repage +460+240 $T \) \( AjaxSpeler/"$LH.jpg" -resize $S -repage +40+125 $T \) \( AjaxSpeler/"$CV.jpg" -resize $S -repage +250+125 $T \)  \( AjaxSpeler/"$RH.jpg" -resize $S -repage +460+125 $T \) \( AjaxSpeler/"$LV.jpg" -resize $S -repage +40+20 $T \) \( AjaxSpeler/"$SP.jpg" -resize $S -repage +250+20 $T \) \( AjaxSpeler/"$RV.jpg" -resize $S -repage +460+20 $T \)  -flatten $NAAM.jpg
#------Eind Systeem 4-3-3-
;;
3-4-3)
#------begin Systeem 3-4-3-
echo $LIJST
 while [ ! -e "AjaxSpeler/$KEEPER.jpg" ]; do echo -e "Wie is keeper($(cat KEEPER)):\c";read KEEPER;if [ "$KEEPER" = "" ]; then KEEPER=$(cat KEEPER);fi;done
 while [ ! -e "AjaxSpeler/$LB.jpg" ]; do echo -e "Wie is LB($(cat LB)):\c";read LB;if [ "$LB" = "" ]; then LB=$(cat LB);fi;done
 while [ ! -e "AjaxSpeler/$B1.jpg" ]; do echo -e "Wie is B1($(cat B1)):\c";read B1;if [ "$B1" = "" ]; then B1=$(cat B1);fi;done
 while [ ! -e "AjaxSpeler/$RB.jpg" ]; do echo -e "Wie is RB($(cat RB)):\c";read RB;if [ "$RB" = "" ]; then RB=$(cat RB);fi;done
 while [ ! -e "AjaxSpeler/$LH.jpg" ]; do echo -e "Wie is LH($(cat LH)):\c";read LH;if [ "$LH" = "" ]; then LH=$(cat LH);fi;done
 while [ ! -e "AjaxSpeler/$B2.jpg" ]; do echo -e "Wie is C1($(cat B2)):\c";read B2;if [ "$B2" = "" ]; then B2=$(cat B2);fi;done
 while [ ! -e "AjaxSpeler/$CV.jpg" ]; do echo -e "Wie is C2($(cat CV)):\c";read CV;if [ "$CV" = "" ]; then CV=$(cat CV);fi;done
 while [ ! -e "AjaxSpeler/$RH.jpg" ]; do echo -e "Wie is RH($(cat RH)):\c";read RH;if [ "$RH" = "" ]; then RH=$(cat RH);fi;done
 while [ ! -e "AjaxSpeler/$LV.jpg" ]; do echo -e "Wie is LV($(cat LV)):\c";read LV;if [ "$LV" = "" ]; then LV=$(cat LV);fi;done
 while [ ! -e "AjaxSpeler/$SP.jpg" ]; do echo -e "Wie is SP($(cat SP)):\c";read SP;if [ "$SP" = "" ]; then SP=$(cat SP);fi;done
 while [ ! -e "AjaxSpeler/$RV.jpg" ]; do echo -e "Wie is RV($(cat RV)):\c";read RV;if [ "$RV" = "" ]; then RV=$(cat RV);fi;done
NAAM="$(date +"%Y%m%d")opstelling"
convert veldwit.png        \( AjaxSpeler/$KEEPER.jpg -resize $S -repage +250+280 $T \) \( AjaxSpeler/"$LB.jpg" -resize $S -repage +40+240 $T \) \( AjaxSpeler/"$B1.jpg" -resize $S -repage +250+180 $T \)  \( AjaxSpeler/"$LH.jpg" -resize $S -repage +40+125 $T \) \( AjaxSpeler/"$B2.jpg" -resize $S -repage +150+125 $T \)  \( AjaxSpeler/"$CV.jpg" -resize $S -repage +360+125 $T \) \( AjaxSpeler/"$RH.jpg" -resize $S -repage +460+125 $T \) \( AjaxSpeler/"$LV.jpg" -resize $S -repage +40+20 $T \) \( AjaxSpeler/"$SP.jpg" -resize $S -repage +250+20 $T \) \( AjaxSpeler/"$RV.jpg" -resize $S -repage +460+20 $T \)  \( AjaxSpeler/"$RB.jpg" -resize $S -repage +460+240 $T \) -flatten $NAAM.jpg
#convert veldwit.png        \( AjaxSpeler/$KEEPER.jpg -resize $S -repage +250+280 $T \) \( AjaxSpeler/"$LB.jpg" -resize $S -repage +40+240 $T \) \( AjaxSpeler/"$B1.jpg" -resize $S -repage +150+220 $T \)  \( AjaxSpeler/"$B2.jpg" -resize $S -repage +360+220 $T \) \( AjaxSpeler/"$RB.jpg" -resize $S -repage +460+240 $T \) \( AjaxSpeler/"$LH.jpg" -resize $S -repage +40+125 $T \) \( AjaxSpeler/"$CV.jpg" -resize $S -repage +250+125 $T \)  \( AjaxSpeler/"$RH.jpg" -resize $S -repage +460+125 $T \) \( AjaxSpeler/"$LV.jpg" -resize $S -repage +40+20 $T \) \( AjaxSpeler/"$SP.jpg" -resize $S -repage +250+20 $T \) \( AjaxSpeler/"$RV.jpg" -resize $S -repage +460+20 $T \)  -flatten $NAAM.jpg
#------Eind Systeem 3-4-3-

;;
5-3-2)
#------begin Systeem 5-3-2-
echo $LIJST
 while [ ! -e "AjaxSpeler/$KEEPER.jpg" ]; do echo -e "Wie is keeper:\c"i;read KEEPER;done
 while [ ! -e "AjaxSpeler/$LB.jpg" ]; do echo -e "Wie is LB:\c";read LB;done
 while [ ! -e "AjaxSpeler/$B1.jpg" ]; do echo -e "Wie is B1:\c";read B1;done
 while [ ! -e "AjaxSpeler/$B3.jpg" ]; do echo -e "Wie is B3:\c";read B3;done
 while [ ! -e "AjaxSpeler/$B2.jpg" ]; do echo -e "Wie is B2:\c";read B2;done
 while [ ! -e "AjaxSpeler/$RB.jpg" ]; do echo -e "Wie is RB:\c";read RB;done
 while [ ! -e "AjaxSpeler/$LH.jpg" ]; do echo -e "Wie is LH:\c";read LH;done
 while [ ! -e "AjaxSpeler/$CV.jpg" ]; do echo -e "Wie is CV:\c";read CV;done
 while [ ! -e "AjaxSpeler/$RH.jpg" ]; do echo -e "Wie is RH:\c";read RH;done
 while [ ! -e "AjaxSpeler/$LV.jpg" ]; do echo -e "Wie is LV:\c";read LV;done
 while [ ! -e "AjaxSpeler/$RV.jpg" ]; do echo -e "Wie is RV:\c";read RV;done
NAAM="$(date +"%Y%m%d")opstelling"
convert veldwit.png        \( AjaxSpeler/$KEEPER.jpg -resize $S -repage +250+300 $T \) \( AjaxSpeler/"$LB.jpg" -resize $S -repage +40+240 $T \) \( AjaxSpeler/"$B1.jpg" -resize $S -repage +150+220 $T \)  \( AjaxSpeler/"$B2.jpg" -resize $S -repage +360+220 $T \) \( AjaxSpeler/"$RB.jpg" -resize $S -repage +460+240 $T \) \( AjaxSpeler/"$LH.jpg" -resize $S -repage +40+125 $T \) \( AjaxSpeler/"$CV.jpg" -resize $S -repage +250+85 $T \)  \( AjaxSpeler/"$RH.jpg" -resize $S -repage +460+125 $T \) \( AjaxSpeler/"$LV.jpg" -resize $S -repage +80+20 $T \) \( AjaxSpeler/"$B3.jpg" -resize $S -repage +250+200 $T \) \( AjaxSpeler/"$RV.jpg" -resize $S -repage +420+20 $T \)  -flatten $NAAM.jpg
#------Eind Systeem 4-3-3-
;;
4-4-2)
#------begin Systeem 4-4-2-
echo $LIJST
 while [ ! -e "AjaxSpeler/$KEEPER.jpg" ]; do echo -e "Wie is keeper:\c"i;read KEEPER;done
 while [ ! -e "AjaxSpeler/$LB.jpg" ]; do echo -e "Wie is LB:\c";read LB;done
 while [ ! -e "AjaxSpeler/$B1.jpg" ]; do echo -e "Wie is B1:\c";read B1;done
 while [ ! -e "AjaxSpeler/$B2.jpg" ]; do echo -e "Wie is B2:\c";read B2;done
 while [ ! -e "AjaxSpeler/$RB.jpg" ]; do echo -e "Wie is RB:\c";read RB;done
 while [ ! -e "AjaxSpeler/$LH.jpg" ]; do echo -e "Wie is LH:\c";read LH;done
 while [ ! -e "AjaxSpeler/$CV.jpg" ]; do echo -e "Wie is CV:\c";read CV;done
 while [ ! -e "AjaxSpeler/$C1.jpg" ]; do echo -e "Wie is C1:\c";read C1;done
 while [ ! -e "AjaxSpeler/$RH.jpg" ]; do echo -e "Wie is RH:\c";read RH;done
 while [ ! -e "AjaxSpeler/$LV.jpg" ]; do echo -e "Wie is LV:\c";read LV;done
 while [ ! -e "AjaxSpeler/$RV.jpg" ]; do echo -e "Wie is RV:\c";read RV;done
NAAM="$(date +"%Y%m%d")opstelling"
convert veldwit.png        \( AjaxSpeler/$KEEPER.jpg -resize $S -repage +250+300 $T \) \( AjaxSpeler/"$LB.jpg" -resize $S -repage +40+240 $T \) \( AjaxSpeler/"$B1.jpg" -resize $S -repage +150+220 $T \)  \( AjaxSpeler/"$B2.jpg" -resize $S -repage +360+220 $T \) \( AjaxSpeler/"$RB.jpg" -resize $S -repage +460+240 $T \) \( AjaxSpeler/"$LH.jpg" -resize $S -repage +40+125 $T \) \( AjaxSpeler/"$CV.jpg" -resize $S -repage +200+125 $T \)  \( AjaxSpeler/"$RH.jpg" -resize $S -repage +460+125 $T \) \( AjaxSpeler/"$LV.jpg" -resize $S -repage +80+20 $T \) \( AjaxSpeler/"$C1.jpg" -resize $S -repage +300+125 $T \) \( AjaxSpeler/"$RV.jpg" -resize $S -repage +420+20 $T \)  -flatten $NAAM.jpg
#------Eind Systeem 4-4-2-
;;
*) echo "Ken ik nog niet";exit;;
esac
if [ $? != 0 ]
then
 echo "Niet goed gegaan. Probeer opnieuw"
 rm -f  $NAAM.jpg
 sleep 3;exit
fi
cp $NAAM.jpg /var/www/html/ajaxfanzone/
#echo "Opstelling klaar. Wie krijgt het gemaild?"
#echo -e "1: Henk 2:Fred 3:Pascal: 4:een ander mailadres:\c"
#read choice
#   case $choice in
#        1) adress="henk@ajaxfanzone.nl";;
#        2) adress="fredmjh@home.nl";;
#        3) adress="pascal@pasynike.nl";;
#        4) echo -e "Geef adres:\c";read ADRES;adress="$ADRES";;
#   esac        
#echo "Opstelling voor wedstrijd. Bijlage opslaan en dan op de site toevoegen." | mailx -s "Opstelling" -a "$NAAM.jpg"  "$adress"
cd /var/www/html/ajaxfanzone>/dev/null
for i in $(ls |grep -v $NAAM);do diff $i $NAAM.jpg >/dev/null ;if [ $? = 0 ]; then :;GEL=1;GNAAM=$i;fi;done
cd - >/dev/null
if [ "$GEL" = "1" ]
then
  echo "Deze opstelling staat waarschijnlijk al op de Ajaxfanzone.nl: zoek naar $GNAAM"
  rm -f /var/www/html/ajaxfanzone/$NAAM.jpg
else
  echo "http://server.pasynike.nl/ajaxfanzone/$NAAM.jpg opslaan op je eigen computer en dan uploaden via wordpress"
echo $KEEPER >KEEPER
echo $LB>LB
echo $B1>B1
echo $B2>B2
echo $RB>RB
echo $LH>LH
echo $CV>CV
echo $RH>RH
echo $LV>LV
echo $SP>SP
echo $RV>RV
fi
sleep 5
